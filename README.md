# mini **database** documention
***
* [action](#action)
    1. [createDB](#createdb)
    2. [insert and update](#insert--update)
    3. [search and delete](#search--delete)
* [examples](#examples)
    1. [create database](#create-database)
    2. [insert data](#insert-data)
    3. [search data](#search-data)
***
All action should be in json form and should have a variable named *action*.\
*action* is the work you want from the database

## action

- createDB
- insert
- search
- delete
- update
***
for all actions except *createDB* you should have another variable named *DB_Name*.\
It is the name of the database name that you want to do work with it.

***
### search & delete

In these actions you should hava *primaryValue* variable that show the primary value of your element in the database.\
For String and character primary value, it should contain a string.\
And for numbers it should contain number not string.

***

### createDB

In this action you should have variable named *name* that contain the name of the databse you want to create.\
And should have *primaryKey* variable that show the number of the primary field.\
And should have *fields* variable that should be array of json object that each object\
contain one table field information of the database.

>Every json in the *fields* array should have :
>>1) *name* that contain name of the field
>>2) *type* that contain type of the field values
>>>types are
>>>>- String
>>>>- int
>>>>- double
>>>>- char

If the type is String another variable name *size* should be exist that declare maximum size of the string.
***

### insert & update

They should have a *data* that contain json object that represent datas of the table fields of the database.\
*data* should contain the name of the fields as the variable name and should contain the value of the fields.\
For *update* action another variable is needed:\
    A variable named *primaryValue* that contain the primary value of that element in the table.
***
***
## Examples
 ### create database
```
{
  "action": "createDB",
  "name": "testDB",
  "primaryKey": 2,
  "fields": [
    {
      "name": "firstName",
      "type": "String",
      "size": 20
    },
    {
      "name": "lastName",
      "type": "String",
      "size": 20
    },
    {
      "name": "age",
      "type": "int"
    },
    {
      "name": "birthYear",
      "type": "int"
    },
    {
      "name": "birthMonth",
      "type": "int"
    },
    {
      "name": "birthDay",
      "type": "int"
    },
    {
      "name": "score",
      "type": "double"
    },
    {
      "name": "gender",
      "type": "char"
    }
  ]
}
```
 ### insert data
```
{
  "action": "insert",
  "DB_Name": "testDB",
  "data": {
    "firstName": "myFirstName",
    "lastName": "myLastName",
    "age": 100,
    "birthYear": 1900,
    "birthMonth": 1,
    "birthDay": 1,
    "score": 19.75,
    "gender": "m",
  }
}
```
 ### search data
```
{
  "action": "search",
  "DB_Name": "testDB",
  "primaryValue": "myLastName"
}
```