package sbu.ap.dataBase;

import sbu.ap.dataBase.engine.Engine;
import sbu.ap.jsonParser.json;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class DatabaseTest {
    public static void main(String[] args) throws Exception {
        int n = new Scanner(System.in).nextInt();
        if(n == 1) {
            File file = new File("test.json");
            Scanner in = new Scanner(file);
            StringBuilder action = new StringBuilder();
            while (in.hasNext())
                action.append(in.nextLine()).append('\n');
            Engine e = new Engine("/home/javad/Desktop/tst");
            json j = e.start(json.parser(action.toString()));
            System.out.println(j.toString(true));
        }
        else if(n == 2) {
            File f = new File("/home/javad/Desktop/database/jvd/testDB/index");
            RandomAccessFile data = new RandomAccessFile(f, "rw");
            while (data.getFilePointer() < data.length()) {
                byte[] str = new byte[20];
                data.read(str);
                System.out.println(new String(str));
                System.out.println(data.readLong());
            }
        }
    }
}
