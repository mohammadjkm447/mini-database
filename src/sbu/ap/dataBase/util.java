package sbu.ap.dataBase;

import sbu.ap.jsonParser.json;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Scanner;

public class util {
    public static boolean useMemory(long length){
        long freeMemory = Runtime.getRuntime().freeMemory();
        long totalMemory = Runtime.getRuntime().totalMemory();
        return (totalMemory - (freeMemory - length)) < 3*totalMemory/4;
    }
    public static String cd(String directory, String fileName){
        if(System.getProperty("os.name").contains("Windows")){
            return directory+"\\"+fileName;
        }
        return directory+"/"+fileName;
    }

    public static json inputJson(InputStream in) throws IOException {
        byte[] byteInt = new byte[4];
        in.read(byteInt);
        int size = ByteBuffer.wrap(byteInt).getInt();
        byte[] byteStr = new byte[size];
        in.read(byteStr);
        String jsonStr = new String(byteStr);
        return json.parser(jsonStr);
    }

    public static void outJson(OutputStream out, json data) throws IOException {
        byte[] jsonByte = data.toString(false).getBytes();
        byte[] sizeByte = ByteBuffer.allocate(4).putInt(jsonByte.length).array();
        out.write(sizeByte);
        out.write(jsonByte);
        out.flush();
    }


}
