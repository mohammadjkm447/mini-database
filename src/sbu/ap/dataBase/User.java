package sbu.ap.dataBase;

import sbu.ap.dataBase.engine.Engine;
import sbu.ap.jsonParser.json;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class User {
    private String path;
    File userInfo;
    json users;

    public User(String path) throws IOException {
        this.path = path;
        File f = new File(path);
        if(!f.exists())
            f.mkdir();
        File userInfo = new File(util.cd(path, "users.json"));
        if(!userInfo.exists())
            userInfo.createNewFile();
        this.userInfo = userInfo;
        if(userInfo.length() == 0){
            users = new json();
            return;
        }
        Scanner in = new Scanner(userInfo);
        StringBuilder str =  new StringBuilder();
        while (in.hasNext())
            str.append(in.nextLine() +'\n');
        users = json.parser(str.toString());
    }

    public Engine singIn(json entry) throws Exception {
        if(entry.contain("action",false)) {
            try {
                createUser(entry);
            }catch (Exception e){
                return null;
            }
            String name = (String)entry.getDataUsingDot("name");
            return getEngine(name);
        }
        String name = (String)entry.getDataUsingDot("name");
        String passwd = (String)entry.getDataUsingDot("password");
        if(checkPassword(name, passwd))
            return getEngine(name);
        return null;

    }

    private Engine getEngine(String name){
        String userPath = util.cd(path, name);
        File userDirectory = new File(userPath);
        if(!userDirectory.exists())
            userDirectory.mkdir();
        return new Engine(userPath);
    }

    public void createUser(json entry) throws Exception {
        String name = (String)entry.getDataUsingDot("name");
        String passwd = (String)entry.getDataUsingDot("password");
        if(isExist(name))
            throw new Exception("user already exist");
        users.put(name, passwd);
        try {
            PrintWriter out = new PrintWriter(userInfo);
            out.println(users.toString(true));
            out.close();
        }catch (Exception ignore){ignore.printStackTrace();}
    }
    private boolean isExist(String name){
        if(users.contain(name, false))
            return true;
        return false;
    }

    public boolean checkPassword(String name, String passwd){
        try {
            String p = (String)users.getDataUsingDot(name);
            if(p.equals(passwd))
                return true;
        }catch (Exception ignore){}
        return false;
    }
}
