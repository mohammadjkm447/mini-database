package sbu.ap.dataBase;

import java.io.File;

public class Lang {
    public static final String db_FileName = "database";
    public static final String indexFileName = "index";
    public static final String tableInfoFileName = "tableInfo";
    public static final char nullChar = '%';
    public static int port = 8888;
    public static final String validateStr = "miniDB_ap98";
    public static final String baseFilesPath;



    static{
        String documentPath = System.getProperty("user.home");
        baseFilesPath = util.cd(documentPath, "mini_database");
        File f = new File(baseFilesPath);
        if(!f.exists()){
            if(!f.mkdir()){
                System.exit(1);
            }
        }
    }
}