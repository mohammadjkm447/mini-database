package sbu.ap.dataBase;

import sbu.ap.dataBase.net.Client;
import sbu.ap.dataBase.net.Server;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args){

        if(args.length == 0){
            argumentHelp();
            System.out.println("But we now start program with ip 127.0.0.1");
            Client client1 = new Client("localhost");
            Server server = new Server();
            Thread c1 = new Thread(client1);
            Thread s = new Thread(server);
            s.start();
            c1.start();
            return;
        }
        List<String> argsList = Arrays.asList(args);
        if(argsList.contains("-p")) {
            int portIndex = argsList.indexOf("-p")+1;
            Lang.port = Integer.parseInt(argsList.get(portIndex));
        }
        if(argsList.contains("-c")){
            String ip;
            if(!argsList.contains("-i")){
                argumentHelp();
                return;
            }
            ip = argsList.get(argsList.indexOf("-i")+1);
            Client c = new Client(ip);
            System.out.println("Client started with "+ip+" ip and "+Lang.port+" port." );
            new Thread(c).start();
            return;
        }
        else if(argsList.contains("-s")){
            Server s = new Server();
            System.out.println("Server started with "+Lang.port+" port.");
            new Thread(s).start();
            return;
        }
        else {
            argumentHelp();
            return;
        }
    }

    public static void argumentHelp(){
        System.out.println("Wrong argument used");
        System.out.println("to set the port user -p");
        System.out.println("to start server use -s");
        System.out.println("to start Client use -c but you should entered the ip using -i");
    }

}
