package sbu.ap.dataBase.data;

public class IndexField<T> {
    private T primaryValue;
    private long position;

    public IndexField(){}

    public IndexField(T primaryValue, long position) {
        this.primaryValue = primaryValue;
        this.position = position;
    }


    public T getPrimaryValue() {
        return primaryValue;
    }

    public void setPrimaryValue(T primaryValue) {
        this.primaryValue = primaryValue;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }
}
