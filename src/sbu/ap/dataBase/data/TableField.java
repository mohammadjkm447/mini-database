package sbu.ap.dataBase.data;

import java.io.Serializable;

public class TableField implements Serializable {
    private String name;
    private FieldType fieldType;
    private int size;

    public TableField(String name, FieldType fieldType, int size) throws Exception {
        this.name = name;
        this.fieldType = fieldType;
        if (fieldType == FieldType.INTEGER) {
            this.size = 4;
        } else if (fieldType == FieldType.DOUBLE){
            this.size = 8;
        }else if(fieldType == FieldType.CHAR){
            this.size = 1;
        }
        else this.size = size;
    }

    public String getName() {
        return name;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public int getSize() {
        return size;
    }
}

