package sbu.ap.dataBase.data;

import com.sun.corba.se.impl.io.TypeMismatchException;
import sbu.ap.dataBase.Lang;

import javax.naming.SizeLimitExceededException;
import java.util.ArrayList;

public class TableElement {
    private ArrayList<Object> fieldsValue;

    public TableElement(){
        fieldsValue = new ArrayList<>();
    }

    public void add(Object o, TableField tableField) throws SizeLimitExceededException,TypeMismatchException{
        if(tableField.getFieldType() == FieldType.STRING) {
            String str = (String) o;
            if(str.length() > tableField.getSize())
                throw new SizeLimitExceededException("given string is more than the field size");
            StringBuilder spaces = new StringBuilder("");
            for (int index = str.length(); index < tableField.getSize(); index++)
                spaces.append(Lang.nullChar);
            fieldsValue.add(str+spaces.toString());
        }
        else if(tableField.getFieldType() == FieldType.CHAR){
            if(o instanceof Character){
                fieldsValue.add((char)o);
            }else {
                if (((String) o).length() != 1) {

                }
                fieldsValue.add(((String) o).charAt(0));
            }
        }
        else if(tableField.getFieldType() == FieldType.INTEGER) {
            if(o instanceof Integer)
                fieldsValue.add((int)o);
            else {
                double d = (double) o;
                if (d != (int) d)
                    throw new TypeMismatchException("inserted double as int");
                fieldsValue.add((int) d);
            }
        }else
            fieldsValue.add(((double)o)+0.0);
    }

    public String getPrimaryValue(int index){
        Object o = fieldsValue.get(index);
        if(o instanceof String)
            return (String)o;
        else if(o instanceof Character)
            return Character.toString((char)o);
        else if(o instanceof Double)
            return Double.toString((double) o);
        else
            return Integer.toString((int)o);
    }

    public Object getFieldValue(int i) {
        return fieldsValue.get(i);
    }
    public ArrayList<Object> getFieldsValue(){
        return fieldsValue;
    }
}
