package sbu.ap.dataBase.data;

import java.io.Serializable;
import java.util.ArrayList;

public class Table implements Serializable {
    private ArrayList<TableField> tableFields;
    private int primaryKey;

    public Table(ArrayList<TableField> tableFields, int primaryKey) {
        this.tableFields = tableFields;
        this.primaryKey = primaryKey;
    }

    public int getPrimaryKey() {
        return primaryKey;
    }

    public TableField getPrimaryField(){
        return tableFields.get(primaryKey-1);
    }

    public ArrayList<TableField> getTableFields() {
        return tableFields;
    }

    public int getTableSize(){
        int size=0;
        for (TableField field : tableFields)
            size += field.getSize();
        return size;
    }
}
