package sbu.ap.dataBase.data;

import sbu.ap.dataBase.fileProcess.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Database {
    private String name;
    private Table table;
    private IndexProcess indexProcess;
    private DataFileProcess dataProcess;

    public Database(String name, Table table, File db, File index) {
        this.name = name;
        this.table = table;
        try {
            dataProcess = new DataFileProcess(db);
        }catch (FileNotFoundException ignore){}
        indexProcess = IndexProcess.newIndexProcess(index, table.getPrimaryField().getSize(), table.getPrimaryField().getFieldType());


    }
    public String getName() {
        return name;
    }

    public ArrayList<TableField> getTableFields(){
        return table.getTableFields();
    }
    public FieldType getPrimaryType(){
        return table.getPrimaryField().getFieldType();
    }

    public boolean delete(Object primaryValue) throws IOException {
        long position = indexProcess.delete(primaryValue);
        if(position == -1)
            return false;
        dataProcess.delete(position, table.getTableSize());
        return true;
    }

    public TableElement search(Object primaryValue)throws Exception{
        long position = indexProcess.search(primaryValue);
        if(position == -1){
            return null;
        }
        return dataProcess.loadTableElement(position,table.getTableFields());
    }

    public boolean addElement(TableElement tableElement) throws IOException {
        Object primaryValue = tableElement.getFieldValue(table.getPrimaryKey()-1);
        long position = indexProcess.getNewPosition(primaryValue, dataProcess.getLength());
        dataProcess.saveTableElement(position, tableElement, getTableFields());
        if(indexProcess.needTobeLoaded())
            indexProcess = IndexProcess.newIndexProcess((IndexMemoryProcess) indexProcess);
        return false;
    }
}
