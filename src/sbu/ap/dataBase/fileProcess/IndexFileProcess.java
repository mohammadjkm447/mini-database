package sbu.ap.dataBase.fileProcess;

import sbu.ap.dataBase.data.FieldType;

import java.io.File;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class IndexFileProcess extends IndexProcess{
    MappedByteBuffer indexData;
    private long size;

    public IndexFileProcess(File indexFile, int primaryValueSize, FieldType fieldType) {
        super(indexFile, primaryValueSize, fieldType);
        size = indexFile.length();
        FileChannel fileChannel = indexDataAccess.getChannel();
        try {
            indexData = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0 , indexDataAccess.length());
        } catch (IOException e) {}

    }

    public long getSize() {
        return size;
    }

    private void updateStream(){
        FileChannel fileChannel = indexDataAccess.getChannel();
        try {
            indexData = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0 , indexDataAccess.length());
        } catch (IOException e) {}

    }

    @Override
    public long search(Object primaryValue) throws IOException {
        long indexPosition = searchForPositionOfIndex(primaryValue);
        if(indexPosition == -1)
            return -1;
        return indexData.getLong((int) (indexPosition+primaryValueSize));
    }


    protected long searchForPositionOfIndex(Object primaryValue) throws IOException {
        long indexPosition = 0;
        while (indexPosition < indexDataAccess.length()){
            if(indexDataType == FieldType.STRING){
                byte[] byteStr = new byte[primaryValueSize];
                long tmp = indexPosition;
                for(int i = 0 ; i < primaryValueSize ; i++) {
                    byteStr[i] = indexData.get((int) tmp);
                    tmp++;
                }
                if(checkIndexString(byteStr, (String)primaryValue)){
                    return indexPosition;
                }
                indexPosition += primaryValueSize;
            }else if(indexDataType == FieldType.INTEGER){
                int n = indexData.getInt((int) indexPosition);
                if(n == (int)primaryValue)
                    return indexPosition;
                indexPosition+=4;
            }else if(indexDataType == FieldType.DOUBLE){
                double d = indexData.getDouble((int) indexPosition);
                if(d == (double)primaryValue)
                    return indexPosition;
                indexPosition+=8;
            }else { //char dataType
                char c = indexData.getChar((int) indexPosition);
                if(c == (char)primaryValue)
                    return indexPosition;
                indexPosition+=1;
            }
            indexPosition += 8;
        }
        return -1;
    }

    @Override
    public long delete(Object primaryValue) throws IOException {
        long position = searchForPositionOfIndex(primaryValue);
        if(position == -1)
            return -1;
        writePrimaryValue(NULL_VALUE, position);

        return indexData.getLong((int) (position+primaryValueSize));
    }

    @Override
    public long getNewPosition(Object primaryValue, long length) throws IOException {
        long position = searchForPositionOfIndex(NULL_VALUE);
        if(position != -1) {
            writePrimaryValue(primaryValue, position);

            return indexData.getLong((int) (position+primaryValueSize));
        }else{
            position = length;
            indexDataAccess.seek(indexDataAccess.length());
            writePrimaryValue(primaryValue);
            indexDataAccess.writeLong(position);
        }
        updateStream();
        return position;
    }

    private void writePrimaryValue(Object primaryValue, long position){
        if(indexDataType == FieldType.STRING) {
            int p = 0;
            for (byte b : ((String) primaryValue).getBytes()){
                indexData.put((int) (p+position), b);
                p++;
            }
        }
        else if(indexDataType == FieldType.DOUBLE)
            indexData.putDouble((int) position,(double)primaryValue);
        else if(indexDataType == FieldType.INTEGER)
            indexData.putInt((int) position, (int)primaryValue);
        else //char data type
            indexData.putChar((int) position, (char)primaryValueSize);
    }
}
