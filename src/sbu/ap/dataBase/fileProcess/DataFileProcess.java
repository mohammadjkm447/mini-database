package sbu.ap.dataBase.fileProcess;

import sbu.ap.dataBase.data.FieldType;
import sbu.ap.dataBase.data.TableElement;
import sbu.ap.dataBase.data.TableField;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

public class DataFileProcess {
    RandomAccessFile dataFile;

    public DataFileProcess(File databaseFile) throws FileNotFoundException {
        this.dataFile = new RandomAccessFile(databaseFile, "rw");
    }

    public long getLength(){
        try {
            return dataFile.length();
        } catch (IOException e) {
            return -2;
        }
    }


    public void delete(long position, int size){
        byte[] NULL = new byte[size];
        try {
            dataFile.seek(position);
            dataFile.write(NULL);
        } catch (IOException e) {}
    }

    public boolean saveTableElement(long position, TableElement element, ArrayList<TableField> tableFields){
        try {
            dataFile.seek(position);
            for(int index = 0; index < tableFields.size(); index++){
                if(!saveFieldValue(element.getFieldValue(index), tableFields.get(index).getFieldType()))
                    return false;
            }
            return true;
        }catch (Exception e){return false;}
    }
    private boolean saveFieldValue(Object o, FieldType type) {
        try {
            if(type == FieldType.STRING){
                String str = (String) o;
                dataFile.writeBytes(str);
            }else if(type == FieldType.DOUBLE){
                double d = (double)o;
                dataFile.writeDouble(d);
            }else if(type == FieldType.INTEGER){
                int n = (int)o;
                dataFile.writeInt(n);
            }else {
                char c = (char)o;
                dataFile.writeByte(c);
            }
        }catch (IOException e) {
            return false;
        }catch (ClassCastException e){
            return false;
        }
        return true;
    }

    public TableElement loadTableElement(long position, ArrayList<TableField> tableFields) throws Exception {
        try {
            dataFile.seek(position);
            TableElement reasult = new TableElement();
            for(int index = 0 ; index < tableFields.size(); index++) {
                Object o = loadFieldValue(tableFields.get(index));
                try {
                    reasult.add(o, tableFields.get(index));
                }catch (Exception ignore){return null;}
            }
            return reasult;
        }catch (IOException e){
            throw new Exception("cannot read from file?!",e);
        }
    }
    private Object loadFieldValue(TableField tableField) throws IOException {
        if (tableField.getFieldType() == FieldType.STRING) {
            byte[] byteStr = new byte[tableField.getSize()];
            dataFile.read(byteStr);
            return new String(byteStr);
        } else if (tableField.getFieldType() == FieldType.INTEGER) {
            return dataFile.readInt();
        } else if (tableField.getFieldType() == FieldType.DOUBLE) {
            return dataFile.readDouble();
        } else
            return (char)dataFile.readByte();
    }
}
