package sbu.ap.dataBase.fileProcess;

import sbu.ap.dataBase.data.FieldType;
import sbu.ap.dataBase.data.IndexField;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class IndexMemoryProcess extends IndexProcess {
    ArrayList<IndexField> data;


    public IndexMemoryProcess(File indexFile, int primaryValueSize, FieldType indexDataType) {
        super(indexFile, primaryValueSize, indexDataType);
        load();
    }

    private void load(){
        data = new ArrayList<>();
        try {
            while (indexDataAccess.getFilePointer() < indexDataAccess.length()) {
                addIndexField();
            }
        }catch (Exception e){}
    }

    protected IndexMemoryProcess addIndexField(){
        try {
            IndexField field;
            if (indexDataType == FieldType.STRING) {
                field = new IndexField<String>();
                byte[] byteStr = new byte[primaryValueSize];
                indexDataAccess.read(byteStr);
                field.setPrimaryValue(new String(byteStr));
            } else if (indexDataType == FieldType.INTEGER) {
                field = new IndexField<Integer>();
                field.setPrimaryValue(indexDataAccess.readInt());
            } else if (indexDataType == FieldType.DOUBLE) {
                field = new IndexField<Double>();
                field.setPrimaryValue(indexDataAccess.readDouble());
            } else { //char data type
                field = new IndexField<Character>();
                field.setPrimaryValue((char) indexDataAccess.readByte());
            }
            field.setPosition(indexDataAccess.readLong());
            data.add(field);
        }catch (Exception ignore){}
        return this;
    }

    @Override
    public long search(Object primaryValue) throws IOException {
        int index = searchInArrayList(primaryValue);
        if(index == -1)
            return -1;
        return data.get(index).getPosition();
    }

    private int searchInArrayList(Object primaryValue) {
        for(int index = 0 ; index < data.size() ; index++){
            IndexField field = data.get(index);
            if(indexDataType == FieldType.STRING){
                if(checkIndexString(((String)field.getPrimaryValue()).getBytes(), (String)primaryValue))
                    return index;
            }
            else if(indexDataType == FieldType.DOUBLE){
                if((double)field.getPrimaryValue() == (double)primaryValue)
                    return index;
            }
            else if(indexDataType == FieldType.INTEGER){
                if((int)field.getPrimaryValue() == (int)primaryValue)
                    return index;
            }
            else { //char data type
                if((char)field.getPrimaryValue() == (char)primaryValue)
                    return index;
            }
        }
        return -1;
    }

    @Override
    public long delete(Object primaryValue) throws IOException {
        int index = searchInArrayList(primaryValue);
        if(index == -1)
            return -1;
        long position = data.get(index).getPosition();
        data.get(index).setPrimaryValue(NULL_VALUE);
        indexDataAccess.seek ((primaryValueSize + 8)*index);
        writePrimaryValue(NULL_VALUE);
        return position;
    }


    @Override
    public long getNewPosition(Object primaryValue, long length) throws IOException {
        int index = searchInArrayList(NULL_VALUE);
        if(index != -1){
            data.get(index).setPrimaryValue(primaryValue);
            indexDataAccess.seek ((primaryValueSize + 8)*index);
            writePrimaryValue(primaryValue);
            return data.get(index).getPosition();
        }
        else {
            long position = indexDataAccess.length();
            indexDataAccess.seek(position);
            writePrimaryValue(primaryValue);
            indexDataAccess.writeLong(length);
            indexDataAccess.seek(position);
            needTobeLoaded = true;
            return length;
        }
    }
}
