package sbu.ap.dataBase.fileProcess;


import sbu.ap.dataBase.Lang;
import sbu.ap.dataBase.data.FieldType;
import sbu.ap.dataBase.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public abstract class IndexProcess {
    protected File indexFile;
    protected RandomAccessFile indexDataAccess;
    protected FieldType indexDataType;
    protected final Object NULL_VALUE;
    protected int primaryValueSize;
    protected boolean needTobeLoaded = false;

    public IndexProcess(File indexFile, int primaryValueSize, FieldType indexDataType){
        this.indexFile = indexFile;
        try {
            indexDataAccess = new RandomAccessFile(indexFile, "rw");
        } catch (FileNotFoundException ignored) {}
        this.indexDataType = indexDataType;
        if(indexDataType == FieldType.STRING) {
            StringBuilder str = new StringBuilder();
            this.primaryValueSize = primaryValueSize;
            while (primaryValueSize > 0) {
                str.append('@');
                primaryValueSize--;
            }
            NULL_VALUE = str.toString();
        }
        else if(indexDataType == FieldType.INTEGER){
            this.primaryValueSize = Integer.BYTES;
            this.NULL_VALUE = Integer.MAX_VALUE;
        }
        else if(indexDataType == FieldType.DOUBLE){
            this.primaryValueSize = Double.BYTES;
            this.NULL_VALUE = Double.MAX_VALUE;
        }
        else {
            this.primaryValueSize = 2;
            NULL_VALUE = Character.MAX_VALUE;
        }
    }

    public boolean needTobeLoaded(){
        return needTobeLoaded;
    }

    public static IndexProcess newIndexProcess(File indexFile, int primaryValueSize, FieldType indexDataType){
        long size = indexFile.length();
        if(util.useMemory(size))
            return new IndexMemoryProcess(indexFile, primaryValueSize, indexDataType);
        return new IndexFileProcess(indexFile, primaryValueSize, indexDataType);
    }
    public static IndexProcess newIndexProcess(IndexMemoryProcess indexProcess){
        long size = indexProcess.primaryValueSize+8;
        if(util.useMemory(size))
            return indexProcess.addIndexField();
        return new IndexFileProcess(indexProcess.indexFile, indexProcess.primaryValueSize, indexProcess.indexDataType);
    }

    public abstract long search(Object primaryValue) throws IOException;

    public abstract long delete(Object primaryValue) throws IOException;

    public abstract long getNewPosition(Object primaryValue, long length) throws IOException;

    protected void writePrimaryValue(Object primaryValue) throws IOException {
        if(indexDataType == FieldType.STRING) {
            indexDataAccess.writeBytes((String) primaryValue);
            if(((String) primaryValue).length() < primaryValueSize) {
                StringBuilder spaces = new StringBuilder();
                for (int index = ((String)primaryValue).length() ; index < primaryValueSize; index++)
                    spaces.append(Lang.nullChar);
                indexDataAccess.writeBytes(spaces.toString());
            }
        }
        else if(indexDataType == FieldType.DOUBLE)
            indexDataAccess.writeDouble((double)primaryValue);
        else if(indexDataType == FieldType.INTEGER)
            indexDataAccess.writeInt((int)primaryValue);
        else //char data type
            indexDataAccess.writeChar((char)primaryValueSize);
    }

    protected boolean checkIndexString(byte[] byteStr, String str){
        StringBuilder o = new StringBuilder();
        for(int index = str.length(); index < primaryValueSize ; index++)
            o.append(Lang.nullChar);
        return new String(byteStr).equals(str+o.toString());
    }


}
