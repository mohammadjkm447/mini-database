package sbu.ap.dataBase.net;

import sbu.ap.dataBase.Lang;
import sbu.ap.dataBase.util;
import sbu.ap.jsonParser.json;
import sbu.ap.jsonParser.jsonInput;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Client implements Runnable,AutoCloseable{
    Socket socket;
    protected String ip = null;
    Scanner input = new Scanner(System.in);
    InputStream in;
    OutputStream out;
    boolean isConnected = false;

    public Client(String ip){
        this.ip = ip;
    }
    protected Client(){}
    @Override
    public void run() {
        try {
            if(!isConnected)
                connect();
        }
        catch (IOException e) {
            System.out.println("not connected");
            return;
        }
        System.out.println("Connected");
        System.out.println("user sign in:");
        System.out.print("want to create user or want to sign in(c & s):> ");
        char c = input.next().toLowerCase().charAt(0);
        json entry = getUserInfo();
        if(c == 'c')
            entry.put("action","create user");
        try {
            if(!signIn(entry)){
                System.out.println("can,t sing in");
                try {
                    close();
                }
                catch (Exception e){}
                return;
            }
            System.out.println("Singed in");
        } catch (IOException e) {
            System.out.println("ERROR");
            try {
                close();
            }
            catch (Exception ignore){}
            return;
        }
        System.out.print("Enter y if you have something to do:> ");
        while(input.next().toLowerCase().charAt(0) == 'y'){
            json action = getAction();
            try {
                json result = sendActionAndGetResult(action);
                System.out.println("result is:");
                System.out.println(result.toString(true));
            }
            catch (Exception e){
                System.out.println("Error");
                return;
            }
            System.out.print("Enter y if you have something to do:> ");
        }
        try {
            endConnection();
        }catch (Exception ignore){}
    }

    private json getUserInfo(){
        System.out.print("user name:> ");
        String name = input.next();
        System.out.print("password:> ");
        String password = input.next();
        json result= new json();
        result.put("name",name);
        result.put("password",password);
        return result;
    }

    private json getAction(){
        System.out.println("Input EEE in a line to end the input");
        System.out.println("your input:> ");
        StringBuilder jsonStr = new StringBuilder();
        while (true){
            String line = input.nextLine();
            if(line.equals("EEE"))
                break;
            jsonStr.append(line).append('\n');
        }
        return json.parser(jsonStr.toString());
    }

    public void connect() throws IOException {
        socket = new Socket(ip, Lang.port);
        in = socket.getInputStream();
        out = socket.getOutputStream();
        out.write(Lang.validateStr.getBytes());
        out.flush();
        isConnected = true;
    }

    public boolean signIn(json data) throws IOException {
        out.write(data.toString(false).getBytes());
        out.flush();
        if(in.read() == 0)
            return false;
        return true;
    }

    public void endConnection() throws Exception {
        out.write(0);
        close();
    }

    public json sendActionAndGetResult(json action) throws IOException {
        out.write(1);
        util.outJson(out, action);
        out.flush();
        return util.inputJson(in);
    }


    public static void main(String[] args){
        Client client = new Client("localhost");
        client.run();
    }
    @Override
    public void close() throws Exception {
        out.close();
        in.close();
        socket.close();
    }
}
