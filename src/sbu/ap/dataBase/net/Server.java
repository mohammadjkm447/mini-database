package sbu.ap.dataBase.net;

import sbu.ap.dataBase.Lang;
import sbu.ap.dataBase.User;
import sbu.ap.dataBase.engine.Engine;
import sbu.ap.dataBase.util;
import sbu.ap.jsonParser.json;
import sbu.ap.jsonParser.jsonInput;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Server implements Runnable{

    public static void main(){
        try {
            ServerSocket serverSocket = new ServerSocket(Lang.port);
            while (true){
                Socket s = serverSocket.accept();
                ServerService service = new ServerService(s);
                Thread t= new Thread(service);
                t.start();
            }
        } catch (IOException e) {e.printStackTrace();}
    }

    @Override
    public void run() {
        main();
    }
}
class ServerService implements Runnable, AutoCloseable{
    private Socket socket;
    private InputStream in;
    private OutputStream out;
    private PrintStream state;
    public ServerService(Socket socket) {
        state = System.out;
        this.socket = socket;
        try {
            in = socket.getInputStream();
            out = socket.getOutputStream();
        }catch (Exception ignore){}
    }

    @Override
    public void run() {
        try {
            byte[] validateStr = new byte[Lang.validateStr.length()];
            in.read(validateStr);
            if(!Lang.validateStr.equals(new String(validateStr))) {
                state.println("server: not validated");
                state.flush();
                return;
            }
            state.println("server: connected");
            state.flush();
            json entry = jsonInput.input(in);
            User user = null;
            try {
                user = new User(Lang.baseFilesPath);
            }catch (Exception e){
                state.println("server: ERROR in user sign in");
                state.println(e.getMessage());
                state.flush();
                return;
            }
            Engine e = user.singIn(entry);
            if(e == null){
                out.write(0);
                out.flush();
                state.println("server: not a valid user");
                state.flush();
                return;
            }else {
                out.write(1);
                out.flush();
                state.println("server: user signed in");
                state.flush();
            }
            while (in.read() == 1){
                json action = util.inputJson(in);
                json result = e.start(action);
                util.outJson(out, result);
                out.flush();
            }
        }catch (Exception ignore){}
    }


    @Override
    public void close() throws Exception {
        in.close();
        out.close();
        socket.close();
        socket.close();
        state.println("server: disconnected");
        state.close();
    }
}
