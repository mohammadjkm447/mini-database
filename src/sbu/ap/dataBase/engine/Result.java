package sbu.ap.dataBase.engine;

import sbu.ap.dataBase.Lang;
import sbu.ap.dataBase.data.FieldType;
import sbu.ap.dataBase.data.TableElement;
import sbu.ap.dataBase.data.TableField;
import sbu.ap.jsonParser.json;

import java.io.PrintStream;
import java.util.ArrayList;

public class Result {


    public static json showTableElement(TableElement tableElement, ArrayList<TableField> tableFields){
        json data = new json();
        for(int index = 0 ; index < tableFields.size() ; index++){
            Object value = tableElement.getFieldValue(index);
            if(tableFields.get(index).getFieldType() == FieldType.STRING){
                String str = (String)value;
                while (str.charAt(str.length()-1)== Lang.nullChar && str.length() != 1){
                    str = str.substring(0,str.length()-1);
                }
                data.put(tableFields.get(index).getName(), str);
            }else if (tableFields.get(index).getFieldType() == FieldType.CHAR){
                String c = String.valueOf((char)value);
                data.put(tableFields.get(index).getName(), c);
            }
            else
                data.put(tableFields.get(index).getName(), value);
        }
        return data;
    }

    public static json typeDismatchForSearch(String neededType){
        json error = new json();
        error.put("ERROR","can not cast given primary value to the database primary value type");
        error.put("needed type",neededType);
        return error;
    }

    public static json showException(String e){
        json error = new json();
        error.put("ERROR",e);
        return error;
    }
    public static json showDoneAction(String str){
        json data = new json();
        data.put("action",str);
        data.put("statue","done");
        return data;
    }

    public static json notTrueAction(){
        json error = new json();
        error.put("ERROR","not true action");
        return error;
    }

    public static json tableInfoError(){
        json error = new json();
        error.put("ERROR","cannot load table info ");
        return error;
    }

    public static json noDatabaseError(String name){
        json error = new json();
        error.put("Result", "no database with this "+name+" name");
        return error;
    }

    public static json databaseExist(String dbName){
        json error = new json();
        error.put("ERROR","database already exist");
        error.put("databse name",dbName);
        return error;
    }

    public static json cannotCreateFile(String name){
        json error = new json();
        error.put("ERROR","can not create File");
        error.put("file name",name);
        return error;
    }

    public static json cannotFindFiles(String str){
        json error = new json();
        error.put("ERROR","some file does not exist");
        ArrayList<Object> missingFiles = new ArrayList<>();
        if(str.contains(Lang.db_FileName))
            missingFiles.add(Lang.db_FileName);
        if(str.contains(Lang.indexFileName))
            missingFiles.add(Lang.indexFileName);
        if(str.contains(Lang.tableInfoFileName))
            missingFiles.add(Lang.tableInfoFileName);
        error.put("missing files", missingFiles);
        return error;
    }
}

