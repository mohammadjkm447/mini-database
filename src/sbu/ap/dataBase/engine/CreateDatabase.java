package sbu.ap.dataBase.engine;

import sbu.ap.dataBase.Lang;
import sbu.ap.dataBase.data.Database;
import sbu.ap.dataBase.data.FieldType;
import sbu.ap.dataBase.data.Table;
import sbu.ap.dataBase.data.TableField;
import sbu.ap.dataBase.util;
import sbu.ap.jsonParser.json;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class CreateDatabase {
    public static Database createDatabase(String directoryPath, json action) throws IOException, Exception {
        String name = (String) action.getDataUsingDot("name");
        File dbFile = new File(directoryPath, Lang.db_FileName);
        dbFile.createNewFile();
        File indexFile = new File(util.cd(directoryPath, Lang.indexFileName));
        indexFile.createNewFile();
        File tableFile = new File(util.cd(directoryPath, Lang.tableInfoFileName));
        tableFile.createNewFile();
        ArrayList<TableField> tableFields = createTableFields((ArrayList)action.getDataUsingDot("fields"));
        int primaryKey = (int)((double)action.getDataUsingDot("primaryKey"));
        Table table = new Table(tableFields, primaryKey);
        ObjectOutputStream tableSave = new ObjectOutputStream(new FileOutputStream(tableFile));
        tableSave.writeObject(table);
        return new Database(name, table, dbFile, indexFile);
    }


    public static ArrayList<TableField> createTableFields(ArrayList jsonFields) throws Exception {
        ArrayList<TableField> result = new ArrayList<>();
        for(json field : (ArrayList<json>)jsonFields){
            String name = (String)field.getDataUsingDot("name");
            String typeStr = (String) field.getDataUsingDot("type");
            FieldType type;
            int size;
            switch (typeStr){
                case "String":
                    type = FieldType.STRING;
                    size = (int)((double)field.getDataUsingDot("size"));
                    break;
                case "int":
                    type = FieldType.INTEGER;
                    size = 4;
                    break;
                case "double":
                    type = FieldType.DOUBLE;
                    size = 8;
                    break;
                case "char":
                    type = FieldType.CHAR;
                    size = 1;
                    break;
                default:
                    throw new Exception("not valid type");
            }
            result.add(new TableField(name, type, size));
        }
        return result;
    }
}
