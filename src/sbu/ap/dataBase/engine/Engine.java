package sbu.ap.dataBase.engine;

import com.sun.corba.se.impl.io.TypeMismatchException;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import sbu.ap.dataBase.Lang;
import sbu.ap.dataBase.data.*;
import sbu.ap.dataBase.util;
import sbu.ap.jsonParser.json;

import javax.naming.SizeLimitExceededException;
import javax.xml.crypto.Data;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Engine {
    private String basePath;
    File infoFile;
    boolean isDatabasesLoaded;
    ArrayList<Database> databases;

    public Engine(String basePath) {
        this.basePath = basePath;
        infoFile = new File(util.cd(basePath, "info.txt"));
        if(!infoFile.exists()) {
            try {
                infoFile.createNewFile();
            } catch (IOException e) {}
        }
        isDatabasesLoaded = loadDatabases();

    }

    private boolean loadDatabases(){
        Scanner in = null;
        databases = new ArrayList<>();

        try {
            if(infoFile.length() == 0)
                return true;
            in = new Scanner(infoFile);
        } catch (FileNotFoundException e) {}
        try {
            while(in.hasNext()){
                databases.add(findDatabaseFromFiles(in.nextLine()));
            }
        }catch (Exception e) {
            return false;
        }
        return true;
    }


    public json start(json action){
        String whatAction="";
        try {
            whatAction = (String)action.getDataUsingDot("action");
        }
        catch (NotFound e){
            return Result.notTrueAction();
        }catch (Exception ignore){}
        if(whatAction.equals("createDB")){
            return createDB(action);
        }
        Database db = null;
        String dbName = null;
        try {
           dbName = (String) action.getDataUsingDot("DB_Name");
        }
        catch (NotFound e){
            return Result.notTrueAction();
        }
        catch (Exception ignore){}
        try {
            db = findDatabase(dbName);
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
            return Result.tableInfoError();
        }
        catch (IOException e){
            e.printStackTrace();
            return Result.tableInfoError();
        }
        catch (Exception e) {
            return Result.cannotFindFiles(e.getMessage());
        }
        if(db == null){
            return Result.noDatabaseError(dbName);
        }
        switch (whatAction){
            case "insert":
                return insertData(db, action);
            case "search":
                return searchData(db, action);
            case "delete":
                return deleteData(db, action);
            case "update":
                return update(db, action);
        }


        return null;
    }
    public json createDB(json action){
        String name=null;
        try {
            name = (String)action.getDataUsingDot("name");
        }catch (Exception ignore){
            return Result.notTrueAction();
        }
        if(isDatabaseExist(name)){
            return Result.databaseExist(name);
        }
        File dbDirectory =  new File(util.cd(basePath, name));
        if(dbDirectory.exists())
            dbDirectory.delete();
        dbDirectory.mkdir();
        Database database;
        try {
            database = CreateDatabase.createDatabase(dbDirectory.getAbsolutePath(), action);
        }catch (IOException e){
            return Result.showException("IO Result");

        }catch (Exception e){
            return Result.notTrueAction();
        }
        if(!addDatabaseToInfoFile(name)){
            return Result.showException("IO Result in info file");
        }
        if(isDatabasesLoaded == true)
            databases.add(database);
        return Result.showDoneAction("creating database with name "+name);
    }

    private json insertData(Database database, json data){
        TableElement tableElement = new TableElement();
        try {
            ArrayList<TableField> tableFields = database.getTableFields();
            for(int index=0; index < tableFields.size() ; index++){
                TableField field = tableFields.get(index);
                String fieldName = field.getName();
                Object value = data.getDataUsingDot("data."+fieldName);
                tableElement.add(value, field);
            }
        }
        catch (TypeMismatchException e){
            return Result.showException(e.getMessage());
        }
        catch (SizeLimitExceededException e){
            return Result.showException(e.getMessage());
        }
        catch (NotFound e){
            return Result.notTrueAction();
        }
        catch (Exception e) {
            return Result.showException("some fields in the given action missing");
        }
        try {
            database.addElement(tableElement);
        }
        catch (IOException e) {
            return Result.showException("IO error occur");
        }
        return Result.showDoneAction("insert data");
    }

    private json searchData(Database database, json action){
        Object primaryObj = null;
        try {
            primaryObj = action.getDataUsingDot("primaryValue");
        }catch (NotFound e) {
            return Result.notTrueAction();
        }catch (Exception ignore){}
        FieldType type = database.getPrimaryType();
        TableElement tableElement;
        if(type == FieldType.STRING){
            if(!(primaryObj instanceof String)){
                return Result.typeDismatchForSearch("String");
            }
        }
        else if(type == FieldType.CHAR){
            if(!(primaryObj instanceof String) ||  ((String)primaryObj).length() != 1){
                return Result.typeDismatchForSearch("char");
            }

        }
        else if(type == FieldType.INTEGER){
            if(!(primaryObj instanceof Double) || ((double)primaryObj) != (int)((double)primaryObj)){
                return Result.typeDismatchForSearch("int");
            }
            primaryObj = (int)((double)primaryObj);
        }
        else { // double dataType
            if(!(primaryObj instanceof Double)){
                return Result.typeDismatchForSearch("double");
            }
        }
        try {
            tableElement = database.search(primaryObj);
            if(tableElement == null)
                return Result.showException("not element with the given primaryValue");
            return Result.showTableElement(tableElement , database.getTableFields());
        }
        catch (IOException e){
            return Result.showException("IO Result");
        }
        catch (Exception e){
            e.printStackTrace();
            return Result.showException(e.getMessage());
        }
    }

    private json deleteData(Database database, json action){
        Object primaryObj = null;
        try {
            primaryObj = action.getDataUsingDot("primaryValue");
        }catch (NotFound e) {
            return Result.notTrueAction();
        }catch (Exception ignore){}
        FieldType type = database.getPrimaryType();
        if(type == FieldType.STRING){
            if(!(primaryObj instanceof String)){
                return Result.typeDismatchForSearch("String");
            }
        }
        else if(type == FieldType.CHAR){
            if(!(primaryObj instanceof String) ||  ((String)primaryObj).length() != 1){
                return Result.typeDismatchForSearch("char");
            }

        }
        else if(type == FieldType.INTEGER){
            if(!(primaryObj instanceof Double) || ((double)primaryObj) != (int)((double)primaryObj)){
                return Result.typeDismatchForSearch("int");
            }
            primaryObj = (int)((double)primaryObj);
        }
        else { // double dataType
            if(!(primaryObj instanceof Double)){
                return Result.typeDismatchForSearch("double");
            }
        }
        try {
            boolean done = database.delete(primaryObj);
            if(done == false)
                return Result.showException("not element with the given primaryValue");
            return Result.showDoneAction("delete");
        }catch (IOException e){
            return Result.showException("IO ERROR");
        }
    }

    private json update(Database database, json data){
        json deleteState = deleteData(database, data);
        if(!deleteState.contain("statue",false))
            return deleteState;
        json insertState = insertData(database, data);
        if(!insertState.contain("statue",false))
            return insertState;
        return Result.showDoneAction("update");
    }


    private boolean isDatabaseExist(String name){
        try {
            Scanner in = new Scanner(infoFile);
            while (in.hasNext()){
                if(in.nextLine().equals(name))
                    return true;
            }
        } catch (FileNotFoundException e) {}
        return false;
    }

    private boolean addDatabaseToInfoFile(String name){
        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(infoFile,true)));
            out.println(name);
            out.close();
            return true;
        }catch (IOException e){
            return false;
        }
    }

    private Database findDatabase(String name) throws Exception {
        if(isDatabasesLoaded){
            for(int index = 0 ; index < databases.size(); index++){
                Database d = databases.get(index);
                if(d.getName().equals(name))
                    return d;
            }
            return null;
        }
        if(!isDatabaseExist(name))
            return null;
        return findDatabaseFromFiles(name);
    }

    private Database findDatabaseFromFiles(String name) throws ClassNotFoundException,IOException, Exception {
        String db_directory = util.cd(basePath, name);
        File directory = new File(db_directory);
        if(!directory.exists())
            directory.mkdir();
        checkDatabaseFiles(db_directory);
        ObjectInputStream in = new ObjectInputStream (new FileInputStream(util.cd(db_directory, Lang.tableInfoFileName)));
        Table table = (Table)in.readObject();
        File databaseFile = new File(util.cd(db_directory, Lang.db_FileName));
        File indexFile = new File(util.cd(db_directory, Lang.indexFileName));
        Database database = new Database(name, table, databaseFile, indexFile);
        return database;
    }

    private void checkDatabaseFiles(String path) throws Exception{
        String err = "";
        File tableFile = new File(util.cd(path, Lang.tableInfoFileName));
        if(!tableFile.exists())
            err.concat(Lang.tableInfoFileName);
        File indexFile = new File(util.cd(path, Lang.indexFileName));
        if(!indexFile.exists())
            err.concat(Lang.indexFileName);
        File databaseFile = new File(Lang.db_FileName);
        if(!databaseFile.exists())
            err.concat(Lang.db_FileName);
        if(err.equals(""))
            return;
        if(err.contains(Lang.tableInfoFileName))
            throw new Exception(err);
        else if(err.contains(Lang.db_FileName)){
            databaseFile.createNewFile();
            if(err.contains(Lang.indexFileName))
                indexFile.createNewFile();
            else{
                indexFile.delete();
                indexFile.createNewFile();
            }
            throw new Exception(err);
        }
        else if(err.contains(Lang.indexFileName)){
            indexFile.createNewFile();
            databaseFile.delete();
            databaseFile.createNewFile();
            throw new Exception(err);
        }
    }
}
